#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import unittest

from addressbook.source.database import AddressBook, Group


class TestContactBook(unittest.TestCase):

    TEST_DB = 'addressbook.db'

    def setUp(self):
        self.ab = AddressBook(data_source='sqlite:///{}'.format(self.TEST_DB))

    def test_groups(self):

        g1 = self.ab.add_group('Group1')
        g2 = self.ab.add_group('Group2')
        self.assertIsInstance(g1, Group)
        self.assertIsInstance(g2, Group)

        p1 = self.ab.add_person(first_name='Person1', group_id=g1.id)
        p2 = self.ab.add_person(first_name='Person2', group_id=g1.id)
        p3 = self.ab.add_person(first_name='Person3')
        self.ab.add_group_to_person(p3.id, g1.id)
        p4 = self.ab.add_person(first_name='Person4', group_id=g2.id)
        p5 = self.ab.add_person(first_name='Person5')
        self.ab.add_group_to_person(p5.id, g2.id)
        p6 = self.ab.add_person(first_name='Person6', group_id=g1.id)
        self.ab.add_group_to_person(p6.id, g2.id)

        self.assertEqual(len(p1.groups), 1)
        self.assertEqual(len(p4.groups), 1)
        self.assertEqual(len(p6.groups), 2)

        self.assertEqual(p1.groups[0].id, g1.id)
        self.assertEqual(p2.groups[0].id, g1.id)
        self.assertEqual(p3.groups[0].id, g1.id)
        self.assertEqual(p4.groups[0].id, g2.id)
        self.assertEqual(p5.groups[0].id, g2.id)
        self.assertEqual(p6.groups[0].id, g1.id)
        self.assertEqual(p6.groups[1].id, g2.id)

    def test_add_person(self):

        p1 = self.ab.add_person(first_name='Abc', last_name='Def')
        p2 = self.ab.add_person(first_name='Tuv', last_name='Xyz')

        r1 = self.ab.find_person_by_name('ab')
        self.assertEqual(len(r1), 1)
        self.assertEqual(r1[0].person_id, p1.id)

        r2 = self.ab.find_person_by_name('xyz')
        self.assertEqual(len(r2), 1)
        self.assertEqual(r2[0].person_id, p2.id)

    def test_get_all_persons(self):

        g1 = self.ab.add_group('Group1')
        g2 = self.ab.add_group('Group2')

        p1 = self.ab.add_person(first_name='Person1', group_id=g1.id)
        p2 = self.ab.add_person(first_name='Person2', group_id=g1.id)
        p3 = self.ab.add_person(first_name='Person3')
        self.ab.add_group_to_person(p3.id, g1.id)
        p4 = self.ab.add_person(first_name='Person4', group_id=g2.id)
        p5 = self.ab.add_person(first_name='Person5')
        self.ab.add_group_to_person(p5.id, g2.id)
        p6 = self.ab.add_person(first_name='Person6', group_id=g1.id)
        self.ab.add_group_to_person(p6.id, g2.id)

        res = self.ab.get_all_persons()
        self.assertEqual(len(res), 6)
        self.assertEqual(set(map(lambda p: p.id, res)), {p1.id, p2.id, p3.id, p4.id, p5.id, p6.id})

        res = self.ab.get_all_persons(group_id=g1.id)
        self.assertEqual(len(res), 4)
        self.assertEqual(set(map(lambda p: p.id, res)), {p1.id, p2.id, p3.id, p6.id})

        res = self.ab.get_all_persons(g2.id)
        self.assertEqual(len(res), 3)
        self.assertSetEqual(set(map(lambda p: p.id, res)), {p4.id, p5.id, p6.id})

    def test_add_fields(self):

        p = self.ab.add_person(first_name='P', last_name='L')

        ph1 = self.ab.add_phone_number(p.id, '+321212121212')
        ph2 = self.ab.add_phone_number(p.id, '+322121212123')

        self.assertEqual(len(p.phone_numbers), 2)
        self.assertEqual(set(map(lambda ph: ph.id, p.phone_numbers)), {ph1.id, ph2.id})

        em1 = self.ab.add_email_address(p.id, 'abc@example.com')
        em2 = self.ab.add_email_address(p.id, 'abc@work.com')
        self.assertEqual(len(p.email_addresses), 2)
        self.assertSetEqual(set(map(lambda em: em.id, p.email_addresses)), {em1.id, em2.id})
        for em in p.email_addresses:
            if em.id == em2.id:
                self.assertEqual(em.email, 'abc@work.com')

        ad1 = self.ab.add_address(p.id, house_number='100', street_name='Welington')
        ad2 = self.ab.add_address(p.id, house_number='102', street_name='Kiwi', city='Auckland', postal_code='10236',
                                  country='NZ')
        self.assertEqual(len(p.addresses), 2)
        self.assertSetEqual(set(map(lambda ad: ad.id, p.addresses)), {ad1.id, ad2.id})
        for ad in p.addresses:
            if ad.id == ad2.id:
                self.assertEqual(ad.house_number, '102')
                self.assertEqual(ad.country, 'NZ')

    def test_trie_search(self):

        p1 = self.ab.add_person(first_name='Sudhakar', last_name='Sesh')
        p2 = self.ab.add_person(first_name='Sudhanshu', last_name='Seth')
        p3 = self.ab.add_person(first_name='Abhinav', last_name='Ghoankar')

        res = self.ab.find_person_by_name('su se')
        self.assertSetEqual({res[0].person_id, res[1].person_id}, {p1.id, p2.id})

        res = self.ab.find_person_by_name('Abh Gh')
        self.assertEqual(len(res), 1)
        self.assertSetEqual({res[0].person_id}, {p3.id})

        res = self.ab.find_person_by_name('ab hi c')
        self.assertEqual(len(res), 0)

    def test_get_persons_by_email(self):

        p1 = self.ab.add_person(first_name='P1', email='abc@example.com')
        p2 = self.ab.add_person(first_name='P1', email='xyz@example.com')
        p3 = self.ab.add_person(first_name='P1', email='abc@badexample.com')

        res = self.ab.get_persons_by_email('abc@example.com')
        self.assertEqual(len(res), 1)
        self.assertSetEqual({res[0].id}, {p1.id})

        res = self.ab.get_persons_by_email('abc')
        self.assertEqual(len(res), 2)
        self.assertSetEqual({res[0].id, res[1].id}, {p1.id, p3.id})

        self.ab.add_email_address(p2.id, 'abc@example.com')
        res = self.ab.get_persons_by_email('abc@example.com')
        self.assertEqual(len(res), 2)
        self.assertSetEqual({res[0].id, res[1].id}, {p1.id, p2.id})

    def tearDown(self):
        self.ab.close_session()
        if os.path.exists(self.TEST_DB):
            os.remove(self.TEST_DB)


if __name__ == '__main__':
    unittest.main()