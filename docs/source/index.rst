.. Address Book documentation master file, created by
   sphinx-quickstart on Fri Nov 18 23:09:56 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Address Book's documentation!
========================================

Contents:

.. autoclass:: database.AddressBook
 :members:
