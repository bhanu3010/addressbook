#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pytrie import SortedTrie as Trie


class TrieSearch(object):
    def __init__(self):
        self.trie = Trie()

    def add_person_to_trie(self, name, user):
        name = name.lower()
        if name in self.trie.keys():
            self.trie[name].update(user)
        else:
            self.trie[name] = user

    def remove_person_from_trie(self, name, person_id):
        name = name.lower()
        if name in self.trie.keys():
            persons = self.trie[name]
            persons.pop(person_id, None)
            if persons:
                self.trie[name] = persons
            else:
                del self.trie[name]
        else:
            raise KeyError('Invalid key search: ', name)

    def add_person(self, person):
        user = {person.id: person.full_name}
        if person.first_name:
            self.add_person_to_trie(person.first_name, user)
        if person.last_name:
            self.add_person_to_trie(person.last_name, user)

    def remove_person(self, person):
        if person.first_name:
            self.remove_person_from_trie(person.first_name, person.id)
        if person.last_name:
            self.remove_person_from_trie(person.last_name, person.id)

    def get_persons_by_prefix(self, prefix):
        prefix = prefix.lower()
        return self.trie.values(prefix)

class SearchValue(object):

    __slots__ = ('person_id', 'full_name')

    def __init__(self, person, full_name):
        self.person_id = person
        self.full_name = full_name

    def __eq__(self, other):
        return self.person_id == other.person_id

    def __hash__(self):
        return hash(self.person_id)

    def __repr__(self):

        return '<SearchValue(person_id={}, full_name={})>'.format(
            self.person_id, self.full_name)