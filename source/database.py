#!/usr/bin/env python
# -*- coding: utf-8 -*-

import functools
import inspect

from sqlalchemy import create_engine, Table
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.exc import SQLAlchemyError, InvalidRequestError

from search import TrieSearch, SearchValue

Base = declarative_base()


class Person(Base):
    """Class represents main person table in AddressBook DB
    """
    __tablename__ = 'persons'

    # Columns
    id = Column(Integer, primary_key=True)
    first_name = Column(String(length=256))
    last_name = Column(String(length=256))
    # Relations
    addresses = relationship("Address", back_populates="person", lazy="joined", cascade="all, delete-orphan")
    phone_numbers = relationship("Phone", back_populates="person", lazy="joined", cascade="all, delete-orphan")
    email_addresses = relationship("Email", back_populates="person", lazy="joined",
                                   cascade="all, delete-orphan")
    groups = relationship("Group", secondary="person_groups", lazy="joined", back_populates="persons")

    @property
    def full_name(self):
        return ' '.join([name for name in [self.first_name, self.last_name]
                         if name is not None])

    def __repr__(self):
        return '<Person({}, Phone numbers: {}, Email addresses: {}, Addresses: {}, Groups: {})'.format(
            self.full_name, self.phone_numbers, self.email_addresses, self.addresses, self.groups)


class Group(Base):
    """Class represents 'Groups' associated with persons in AddressBook DB
    """
    __tablename__ = 'groups'
    # Columns
    id = Column(Integer, primary_key=True)
    name = Column(String(length=256))
    # Relations
    persons = relationship("Person", secondary='person_groups', lazy="joined", back_populates="groups")

    def __repr__(self):
        return '<Group(name={})'.format(self.name)


class ReferencePerson(Base):
    """An auxiliary class helping to add personID attribute to all the other ancillary tables
    """
    __abstract__ = True

    @declared_attr
    def person_id(cls):
        return Column(Integer, ForeignKey('persons.id'))

    id = Column(Integer, primary_key=True)


class Address(ReferencePerson):
    """Class representing all addresses in AddressBook DB
    """
    __tablename__ = 'addresses'
    # Columns
    house_number = Column(String(length=32))
    street_name = Column(String(length=128))
    city = Column(String(length=128))
    country = Column(String(length=128))
    postal_code = Column(String(length=32))
    # Relations
    person = relationship("Person", back_populates="addresses", single_parent=True, lazy="joined",
                          cascade="all, delete-orphan")

    def __repr__(self):
        return '<Address> {}>'.format(', '.join([address for address in [self.street_name, self.house_number,
                                                                         self.city, self.country, self.postal_code]
                                                 if address is not None]))


class Phone(ReferencePerson):
    """Class representing Phone numbers in AddressBook DB"""
    __tablename__ = 'phone_numbers'
    # Columns
    phone = Column(String(length=256), nullable=False)
    # Relations
    person = relationship("Person", back_populates="phone_numbers", single_parent=True, lazy="joined",
                          cascade="all, delete-orphan")

    def __repr__(self):
        return '<PhoneNumber(number={})'.format(self.phone)


class Email(ReferencePerson):
    """Class representing all Email addresses in AddressBook DB"""
    __tablename__ = 'email_addresses'

    # Columns
    email = Column(String(length=256), nullable=False)
    # Relations
    person = relationship("Person", back_populates="email_addresses", single_parent=True, lazy="joined",
                          cascade="all, delete-orphan")

    def __repr__(self):
        return '<EmailAddress(address={})>'.format(self.email)


class PersonGroup(Base):
    """
    Auxiliary table to setup many-many relationships between persons and groups tables
    through a composite primary key
    """
    __tablename__ = 'person_groups'

    # (person_id, group_id) is the composite primary key for this table.
    person_id = Column(Integer, ForeignKey('persons.id'), primary_key=True)
    group_id = Column(Integer, ForeignKey('groups.id'), primary_key=True)


def bootstrap_sa_session_handler(func):
    """SQLAlchemy func decorator used to bootstrap all methods of AddressBook with an augmented
    session handler"""

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        self = args[0]
        try:
            res = func(*args, **kwargs)
            if any([x in str(func) for x in ['add_', 'delete_']]):
                self.session.commit()
        except SQLAlchemyError as e:
            self.session.rollback()
            raise
        return res

    return wrapper


def install_decorator(cls):
    """AddressBook class decorator to setup custom session handler for all the methods"""
    for name, func in inspect.getmembers(cls, inspect.ismethod):
        if not name.startswith('__'):
            setattr(cls, name, bootstrap_sa_session_handler(func))
    return cls


@install_decorator
class AddressBook(object):
    """Main AddressBook class responsible for handling all user requests"""

    def __init__(self, data_source=None):
        sa_engine = create_engine(data_source)
        self.sa_sessionmaker = sessionmaker(bind=sa_engine)
        Base.metadata.create_all(sa_engine)
        self.session = sessionmaker(bind=sa_engine)()
        self.search = TrieSearch()

    def add_person(self, first_name=None, last_name=None, group_id=None, phone_number=None, email=None,
                   house_number=None, street_name=None, city=None, country=None, postal_code=None):
        """

        Adds a person to address book

        :param first_name: First name
        :type first_name: str
        :param last_name: Last name
        :type last_name: str
        :param phone_number: Phone number
        :type phone_number: str
        :param email_address: Email address
        :type email_address: str
        :param group_id: id of the group this person belongs to
        :type group_id: int
        :param house_number: House number
        :type house_number: int
        :param street_name: Street Name
        :type street_name: str
        :param city: City Name
        :type city: str
        :param postal_code: Postal Code
        :type postal_code: int
        :return: Person created
        :rtype: Person
        """
        name_attrs = [first_name, last_name]
        if any(name_attrs):
            person = Person(first_name=first_name, last_name=last_name)
            self.session.add(person)
            self.session.commit()
            if phone_number:
                self.add_phone_number(person.id, phone_number)
            if email:
                self.add_email_address(person.id, email)
            if group_id:
                group_id = self.session.query(Group).get(group_id)
                if group_id is None:
                    raise InvalidRequestError('Invalid {} object {} passed to {}'.format('Group', group_id,
                                                                                 inspect.stack()[0][3]))
                person.groups.append(group_id)
            address = [house_number, street_name, city, country, postal_code]
            if any(address):
                self.add_address(person.id, *address)
            self.search.add_person(person)
            return person
        else:
            raise ValueError('At least one of first_name and last_name must be specified')

    def add_group(self, name):
        """
        Add a group to the AddressBook DB

        :param name: Name of the group
        :type name: str
        :return: Group created
        :rtype Group

        """
        if not name:
            raise ValueError('Group name must be specified')
        group = Group(name=name)
        self.session.add(group)
        return group

    def get_person_by_id(self, person_id):
        """Get person details by ID

        :param person_id: PersonID of user
        :type person_id: int
        :return: Person associated with personID
        :rtype: Person

        """
        person = self.session.query(Person).get(person_id)
        if person is None:
            raise InvalidRequestError('Invalid {} object {} passed to {}'.format('Person', person_id,
                                                                                 inspect.stack()[0][3]))
        return person

    def get_all_persons(self, group_id=None):
        """Get all persons details in AddressBook associated with a group

        :param group_id: GroupID
        :type group_id: int
        :return: Users of the group
        :rtype: list
        """
        query = self.session.query(Person)
        if group_id:
            query = query.filter(Person.groups.any(id=group_id))
        result = query.all()
        return result

    def get_persons_by_email(self, email):
        """
        Get all persons by mail address

        :param email: EmailAddress
        :type email: str
        :return: Users matching the passed emailaddress
        :rtype: list
        """
        return self.session.query(Person).filter(Person.email_addresses.any(
            Email.email.like('{}%'.format(email)))).all()

    def get_group_by_id(self, group_id):
        """
        Get group details by ID

        :param group_id: GroupID
        :type group_id: int
        :return: Group associated with groupID
        :rtype: Group
        """
        group = self.session.query(Group).get(group_id)
        if group is None:
            raise InvalidRequestError('Invalid {} object {} passed to {}'.format('Group', group_id,
                                                                                 inspect.stack()[0][3]))
        return group

    def add_group_to_person(self, person_id, group_id):
        """
        Add a group to a person

        :param person_id: PersonID
        :type person_id: int
        :param group_id: GroupID
        :type group_id: int
        :return: None
        :rtype: None
        """
        person = self.session.query(Person).get(person_id)
        if person is None:
            raise InvalidRequestError('Invalid {} object {} passed to {}'.format('Person', person_id,
                                                                                 inspect.stack()[0][3]))
        group = self.session.query(Group).get(group_id)
        if group is None:
            raise InvalidRequestError('Invalid {} object {} passed to {}'.format('Group', group_id,
                                                                                 inspect.stack()[0][3]))
        person.groups.append(group)

    def add_phone_number(self, person_id, phone_number):
        """
        Add phone number to a person

        :param person_id: PersonID
        :type person_id: int
        :param phone_number: Phone Number
        :type phone_number: int
        :return: Phone inserted
        :rtype: Phone
        """
        if not phone_number:
            raise ValueError('Phone number must be specified')
        person = self.session.query(Person).get(person_id)
        if person is None:
            raise InvalidRequestError('Invalid {} object {} passed to {}'.format('Person', person_id,
                                                                                 inspect.stack()[0][3]))
        phone = Phone(person=person, phone=phone_number)
        self.session.add(phone)
        return phone

    def add_email_address(self, person_id, email_address):
        """
        Add an email address to a person

        :param person_id: PersonID
        :type person_id: int
        :param email_address: Email Address
        :type email_address: str
        :return: Email Address inserted
        :rtype: Email

        """
        if not email_address:
            raise ValueError('Email address must be specified')
        person = self.session.query(Person).get(person_id)
        if person is None:
            raise InvalidRequestError('Invalid {} object {} passed to {}'.format('Person', person_id,
                                                                                 inspect.stack()[0][3]))
        email = Email(person=person, email=email_address)
        self.session.add(email)
        return email

    def add_address(self, person_id, house_number=None, street_name=None, city=None, country=None,
                    postal_code=None):
        """Add an address to a person

        :param person_id: PersonID
        :type person_id: int
        :param house_number: House Number
        :type house_number: int
        :param street_name: Street Name
        :type street_name: str
        :param city: City Name
        :type city: str
        :param postal_code: Postal Code
        :type postal_code: int
        :return: New address inserted
        :rtype: Address
        """
        if not any([house_number, street_name, city, postal_code, country]):
            raise ValueError('At least one address field must be specified')
        person = self.session.query(Person).get(person_id)
        if person is None:
            raise InvalidRequestError('Invalid {} object {} passed to {}'.format('Person', person_id,
                                                                                 inspect.stack()[0][3]))
        address = Address(person=person, house_number=house_number, street_name=street_name,
                          city=city, postal_code=postal_code, country=country)
        self.session.add(address)
        return address

    def delete_person(self, person_id):
        """
        Delete person from Address Book

        :param person_id: PersonID
        :type person_id: int
        :return: None
        :rtype: None
        """
        person = self.session.query(Person).get(person_id)
        if person is None:
            raise InvalidRequestError('Invalid {} object {} passed to {}'.format('Person', person_id,
                                                                                 inspect.stack()[0][3]))
        self.session.delete(person)
        self.session.commit()
        self.search.remove_person(person)

    def delete_group(self, group_id):
        """
        Delete group from Address Book

        :param group_id: GroupID
        :type group_id: int
        :return: None
        :rtype: None
        """
        group = self.session.query(Group).get(group_id)
        if group is None:
            raise InvalidRequestError('Invalid {} object {} passed to {}'.format('Group', group_id,
                                                                                 inspect.stack()[0][3]))
        self.session.delete(group)

    def delete_phone_number(self, phone_number_id):
        """
        Delete phone number and associated persons from AddressBook

        :param phone_number_id: Phone number ID
        :type phone_number_id: int
        :return: None
        :rtype: None
        """
        phone = self.session.query(Phone).get(phone_number_id)
        if phone is None:
            raise InvalidRequestError('Invalid {} object {} passed to {}'.format('Phone', phone_number_id,
                                                                                 inspect.stack()[0][3]))
        self.session.delete(phone)

    def delete_email_address(self, email_address_id):
        """
        Delete Email and associated person from Address Book

        :param email_address_id: Email Address ID
        :type email_address_id: int
        :return: None
        :rtype: None
        """
        email = self.session.query(Email).get(email_address_id)
        if email is None:
            raise InvalidRequestError('Invalid {} object {} passed to {}'.format('Email', email_address_id,
                                                                                 inspect.stack()[0][3]))
        self.session.delete(email)

    def delete_address(self, address_id):
        """
        Delete Address and associated person from Address Book

        :param address_id: Address ID
        :type address_id: int
        :return: None
        """
        address = self.session.query(Address).get(address_id)
        if address is None:
            raise 'Invalid {} object {} passed to {}'.format('Email', address_id, inspect.stack()[0][3])
        self.session.delete(address)
        self.session.commit()

    def find_person_by_prefix(self, prefix):
        """
        Fina a person by prefix

        :param prefix: Prefix based on users are fetched
        :type prefix; str
        :return: All the users with a common prefix
        :rtype: list
        """

        result = self.search.get_persons_by_prefix(prefix)
        final_result = {}
        for d in result:
            final_result.update(d)
        return list(map(lambda (k, v): SearchValue(k, v), final_result.items()))

    def find_person_by_name(self, name):
        """
        Fina a person either by first name or last name or both

        :param name: "first-name", "last-name", "first-name last-name"
        :type name: str
        :return: List of users with a matching first or last name
        :rtype: list
        """

        if name and len(name.split()) == 1:
            return self.find_person_by_prefix(name)
        else:
            result_sets = []
            for word in name.split():
                result_sets.append(set(self.find_person_by_prefix(prefix=word)))
            return list(set.intersection(*result_sets))

    def close_session(self):
        self.session.close()