#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'bhanucp'
__version__ = '0.0.1'

from database import AddressBook

__all__ = [AddressBook]