.. image:: https://img.shields.io/badge/AddressBook-v0.0.1-blue.svg   
   :target: https://github.com/bhanu3010/AddressBook
|    
A comprehensive Address Book API built with a capability to:

1. Add a contact to address book
2. Add a group to a contact
3. Add contact auxiliary details like phone, mail and address etc.
4. Get the list of users belonging to a group
5. Get the list of groups a user belongs to
6. Search a user based on firstname/lastname/both

Example Code:
-------------

.. code:: python

   from addressbook.source.database import AddressBook

   ab = AddressBook('sqlite:///test.db')
   ab.add_group('TechGeeks')
   ab.add_person(first_name='John', last_name='Dave')
   ab.add_person(first_name='Christopher', last_name='Joseph', group_id=1)
   ab.add_group_to_person(person_id=1, group_id=1)
   ab.add_email_address(person_id=1, email_address='john.dave@example.com')
   ab.add_email_address(person_id=1, email_address='dave.john@gmail.com')
   ab.add_phone_number(person_id=1, phone_number='+1 304-548-5306')
   ab.add_address(person_id=1, city='Michigan', postal_code=48137,
               street_name='2036 Columbia Mine Road')
   print ab.get_person_by_id(person_id=1)
   <Person(John Dave, Phone numbers: [<PhoneNumber(number=+1 304-548-5306)], Email addresses: [<EmailAddress(address=dave.john@gmail.com)>, <EmailAddress(address=john.dave@example.com)>], Addresses: [<Address> 2036 Columbia Mine Road, Michigan, 48137>], Groups: [<Group(name=TechGeeks)])


For full API details, please refer `Address Book documentation <http://addressbook.readthedocs.io/en/latest/>`_.
